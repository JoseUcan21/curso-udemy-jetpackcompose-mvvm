package com.joseluisucan.composemvvm.config

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Aplicacion:Application()