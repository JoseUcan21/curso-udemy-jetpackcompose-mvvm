package com.joseluisucan.composemvvm.data.network

import com.joseluisucan.composemvvm.data.model.DogResponseModel
import retrofit2.http.GET

interface DogApi {
    @GET("20")
    suspend fun getImages():DogResponseModel
}