package com.joseluisucan.composemvvm.data.repository

import com.joseluisucan.composemvvm.data.model.DogResponseModel
import com.joseluisucan.composemvvm.data.network.DogApi
import javax.inject.Inject

class DogsRepo @Inject constructor(
    private val dogApi: DogApi
){
    suspend fun getDogsImages():DogResponseModel{
        return dogApi.getImages()
    }
}